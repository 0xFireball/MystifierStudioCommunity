/*jshint esversion: 6 */

function saveLastEditedFile(leFn) {
    localStorage.setItem("opts.lastEdited", leFn);
}

function loadLastEditedFile() {
    return localStorage.getItem("opts.lastEdited");
}

function onSaveFile(saveFinishedCallback) {
    if (_currentFileName) {
        //Save file that is already selected
        localStorage.setItem("file." + _currentFileName, jsEditor.getValue());
        saveLastEditedFile(_currentFileName);
        _isUnsaved = false;
        updateStatusBar();
        if (_.isFunction(saveFinishedCallback)) {
            saveFinishedCallback();
        }
    }
    else {
        showModalInput("Save File", "Please enter a file name", function (fn) {
            if (fn) {
                //Save the file to localStorage
                _currentFileName = fn;
                //Call the function again; this time it should save to existing file.
                if (saveFinishedCallback) {
                    onSaveFile(saveFinishedCallback);
                }
                else {
                    onSaveFile();
                }
            }
        });
    }
}

function onSaveFileAs() {
    let previousFile = _currentFileName;
    _currentFileName = null;
    onSaveFile(function () {
        if (!_currentFileName) {
            _currentFileName = previousFile;
        }
    });
}

function loadCurrentFileIntoEditor() {
    if (_currentFileName) {
        jsEditor.setValue(localStorage.getItem("file." + _currentFileName) || "");
    }
}

function onOpenFile() {
    showModalInput("Open File", "Please enter a file name", function (fn) {
        if (fn) {
            //Save the file to localStorage
            _currentFileName = fn;
            //Load the content into the editor
            loadCurrentFileIntoEditor();
            _isUnsaved = false;
            saveLastEditedFile(_currentFileName);
            updateStatusBar();
        }
    });
}

function onCreateNew() {
    _currentFileName = null;
    jsEditor.setValue(""); //Clear editor
    _isUnsaved = true;
    updateStatusBar();
}


//Event handlers

shortcut.add("Ctrl+S", onSaveFile);
$("#mnSave").click(onSaveFile);

shortcut.add("Ctrl+Alt+S", onSaveFileAs);
$("#mnSaveAs").click(onSaveFileAs);

shortcut.add("Ctrl+O", onOpenFile);
$("#mnLoad").click(onOpenFile);

shortcut.add("Alt+N", onCreateNew);
$("#mnNew").click(onCreateNew);

let leFile = loadLastEditedFile();
if (leFile) {
    _currentFileName = leFile;
    loadCurrentFileIntoEditor();
    updateStatusBar();
}