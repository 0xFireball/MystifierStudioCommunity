/*jshint esversion: 6 */
//let msRealm = continuum.createRealm();

$(document).ready(function () {
    $('body').addClass('loaded');
});

let _isUnsaved = false;
let _currentFileName;

let cmOptions = {
    mode: "javascript",
    theme: "solarized dark",
    lineNumbers: true,
    scrollbarStyle: "simple"
};

let jsEditor;
let outputEditor = CodeMirror.fromTextArea(document.getElementById("output-editor"), cmOptions);

jsEditor = CodeMirror.fromTextArea(document.getElementById("editor"), cmOptions);
jsEditor.setSize(null, "100%");

$("#btnObfuscate").click(function () {
    $.post("script/mystifiertool.escx", {
        inputjs: jsEditor.getValue(),
        action: "obfuscate"
    }, function (data, status, xhr) {
        outputEditor.setValue(data);
    }, "text")
        .fail(function () {
            showModalMessage("Error", "Mystifier Studio could not reach the server.");
        });
});

jsEditor.on("change", function (cm, changeObj) {
    _isUnsaved = true;
    updateStatusBar();
});

$("#btnBeautify").click(function () {
    $.post("script/mystifiertool.escx", {
        inputjs: encodeURIComponent(btoa(jsEditor.getValue())),
        action: "beautify"
    }, function (data, status, xhr) {
        jsEditor.setValue(atob(data));
    }, "text")
        .fail(function () {
            showModalMessage("Error", "Mystifier Studio could not reach the server.");
        });
});

$("#btnExecute").click(function () {
    localStorage.setItem("_.execSource", jsEditor.getValue());
    var win = window.open('execute.html', '_blank');
    if (win) {
        //Browser has allowed it to be opened
        win.focus();
    } else {
        //Broswer has blocked it
        showModalMessage("New window blocked", "Your browser blocked the request to open a new tab to run the code. Please set it to allow popups from this site and try again.");
    }
});

new Clipboard('#btnCopy', {
    text: function (trigger) {
        return outputEditor.getValue();
    }
});



function showModalMessage(title, text) {
    let headerCnt = $("#modal-header-content");
    let bodyCnt = $("#modal-body-content");
    headerCnt.html(title);
    bodyCnt.html(text);
    let popup = $('#popupModal');
    popup.on('hidden.bs.modal', function () {
        headerCnt.empty();
        bodyCnt.empty();
    });
    popup.modal('show');
}

function ignoreEnter(event) {
    if (event.keyCode == 13) {
        return false;
    }
}

function updateStatusBar() {
    let statusBar = $("#statusBar");
    let statusBarText;
    if (_currentFileName) {
        statusBarText = (_isUnsaved ? (_currentFileName + "*") : _currentFileName);
    }
    else {
        statusBarText = "[New File]" + (_isUnsaved ? "*" : "");
    }
    statusBar.html(statusBarText);
}

function showModalInput(title, text, inputCallback) {
    let headerCnt = $("#modal-header-content");
    let bodyCnt = $("#modal-body-content");
    headerCnt.html(title);
    bodyCnt.html(text);
    bodyCnt.append('<form onsubmit="return false"><input class="form-control" id="modalInput" type="text" /></form>');
    let popup = $('#popupModal');
    popup.on('hidden.bs.modal', function () {
        var inputtedText = $("#modalInput").val();
        headerCnt.empty();
        bodyCnt.empty();
        inputCallback(inputtedText);
    });
    var inputtedText = $("#modalInput").focus();
    popup.modal('show');
}

shortcut.add("F5", function () {
    $("#btnExecute").click();
});

$("#mnObfuscate").click(function () {
    $("#btnObfuscate").click();
});

$("#mnBeautify").click(function () {
    $("#btnBeautify").click();
});

$("#mnExecute").click(function () {
    $("#btnExecute").click();
});

shortcut.add("Shift+Alt+F", function () { $("#btnBeautify").click(); });

updateStatusBar();