# Mystifier Studio Community
A free, open-source version of [Mystifier Studio](https://zetaphase.io/view.html?p=mys-studio) that runs in your browser.

You can try it out with the official build: https://apps.zetaphase.io/msc/

# Summary
- The entire editor frontend is HTML5/CSS/JS and is completely open source, but the obfuscation and JS formatter are closed source and are provided by a custom FireHTTP Server; these features will not be available if you are self-hosting the application.
- The functionality and UI are based on Mystifier Studio Professional, a desktop application about which details can be found below.
- Obviously, you can't have all the features available in the desktop version in this version of the application, but we are doing our best :D


# About Mystifier Studio Professional
Mystifier Studio is an advanced JavaScript code editor with a built in JavaScript engine and obfuscator! Mystifier Studio utilizes four different obfuscation engines that feed their output to each other, including a renamer, a packer, and a Unicode scrambler. You can develop your code in Mystifier Studio and run it with a built in debugger and JavaScript engine, then obfuscate your code for production with the click of the button. Everything is tightly integrated, so you never have to leave the window to generate a production build of your JavaScript project. Mystifier Studio's enigmatic obfuscation is not easily broken and will protect your intellectual property. Stop worrying and start coding. Get Mystifier Studio for free from [the official product page](https://zetaphase.binpress.com/product/mystifier-studio/3826)!
